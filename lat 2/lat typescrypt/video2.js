var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Role;
(function (Role) {
    Role["ADMIN"] = "ADMIN";
    Role["MAHASISWA"] = "MAHASISWA";
})(Role || (Role = {}));
var BaseUser = /** @class */ (function () {
    function BaseUser() {
        this.id = 0;
        this.nama = "";
    }
    BaseUser.prototype.toString = function () {
        return JSON.stringify(this);
    };
    return BaseUser;
}());
var Mahasiswa = /** @class */ (function (_super) {
    __extends(Mahasiswa, _super);
    function Mahasiswa() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.nim = 0;
        _this.role = Role.MAHASISWA;
        return _this;
    }
    return Mahasiswa;
}(BaseUser));
var Admin = /** @class */ (function (_super) {
    __extends(Admin, _super);
    function Admin() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.nip = 0;
        _this.role = Role.ADMIN;
        return _this;
    }
    return Admin;
}(BaseUser));
var UserService = /** @class */ (function () {
    function UserService(admin, mahasiswa) {
        this.admin = admin;
        this.mahasiswa = mahasiswa;
    }
    UserService.prototype.createLog = function () {
        console.log("Admin", this.admin.toString());
        console.log("Mahasiswa", this.mahasiswa.toString());
    };
    return UserService;
}());
var mahasiswa = new Mahasiswa();
mahasiswa.id = 1;
mahasiswa.nim = 1254;
mahasiswa.nama = "n.fajar";
mahasiswa.role = Role.MAHASISWA;
var admin = new Admin();
admin.id = 2;
admin.nip = 2312;
admin.nama = "josephdp";
admin.role = Role.ADMIN;
var userService = new UserService(admin, mahasiswa);
userService.createLog();
console.log(new Mahasiswa());
console.log(new Admin());
//console.log(userService.merge());
//console.log(mahasiswa);
//console.log(admin);
console.log(mahasiswa.toString());
console.log(admin.toString());
console.log(typeof admin === typeof mahasiswa);
