interface User{
    id: number;
    nama: string;
    role: Role;
}

enum Role{
ADMIN = "ADMIN",
MAHASISWA = "MAHASISWA"
}

abstract class BaseUser implements User{
    
    public id: number = 0;
    
    public nama: string = "";
    
    public abstract role: Role;

    public toString(): string {
        return JSON.stringify(this);
    }

}

class Mahasiswa extends BaseUser{

    public nim: number = 0;

    public role: Role = Role.MAHASISWA;

}

class Admin extends BaseUser {

    public nip: number = 0;

    public role: Role = Role.ADMIN;

}

class UserService {
    private admin: Admin;
    private mahasiswa: Mahasiswa;

    constructor(admin: Admin, mahasiswa: Mahasiswa) {
        this.admin = admin;
        this.mahasiswa = mahasiswa;
    }

    public createLog(): void{
        console.log("Admin", this.admin.toString());
        console.log("Mahasiswa", this.mahasiswa.toString());

    }

    //public merge(): User{
      //  return Object.assign({}, this.admin, this.mahasiswa);
    //}
}



let mahasiswa: Mahasiswa = new Mahasiswa();
mahasiswa.id = 1;
mahasiswa.nim = 1254;
mahasiswa.nama = "n.fajar";
mahasiswa.role = Role.MAHASISWA;

let admin: Admin = new Admin();
admin.id = 2;
admin.nip = 2312;
admin.nama = "josephdp";
admin.role = Role.ADMIN;


const userService: UserService = new UserService(admin, mahasiswa);
userService.createLog();
console.log(new Mahasiswa());
console.log(new Admin());
//console.log(userService.merge());

//console.log(mahasiswa);

//console.log(admin);

console.log(mahasiswa.toString());

console.log(admin.toString());




console.log(typeof admin === typeof mahasiswa);


