import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';

import Login from "../views/Login.vue";
//import Register from "../views/Register.vue";


Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
  path:"/",
  name:"login",
  component:() => import("../views/Login.vue")
  }

];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

export default router
