
import BaseEntity from "@/entity/BaseEntity";

export default class User extends BaseEntity {
    public username: string = "";
    public password: string= "";
    public profileName: string= "";
}